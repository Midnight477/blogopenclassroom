import { post } from 'selenium-webdriver/http';
import { Component, OnInit, Input, } from '@angular/core';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {

  @Input() postTitle: string;
  @Input() postContent: string;
  @Input() postLikeIts: number;
  @Input() postDate: Date;
  
  onLoveIt(){
    this.postLikeIts +=1;
  }

  onDontLoveIt(){
    this.postLikeIts -= 1;
  }

  constructor() { }

  ngOnInit() {
  }



}
